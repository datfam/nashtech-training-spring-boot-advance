package com.nashtech.authorization.server.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Value("${security.oauth2.user.username}")
    private String username;
    
    @Value("${security.oauth2.user.password}")
    private String password;
    
    @Value("${security.oauth2.user.roles}")
    private String[] roles;
    
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests()
		        .antMatchers("/oauth/token").permitAll()
			  	.antMatchers("/oauth/check_token").permitAll()
                .anyRequest().authenticated();
    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    	/* setting user is stored in memory */
        auth.inMemoryAuthentication()
            .withUser(username)
            .password(new BCryptPasswordEncoder().encode(password))
            .roles(roles);
    }
}