package com.nashtech.training.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	@Override
    @Order(Ordered.HIGHEST_PRECEDENCE)
	public void configure(HttpSecurity http) throws Exception {
		http
		.sessionManagement()
		.sessionCreationPolicy(SessionCreationPolicy.ALWAYS)
		.and()
		.csrf().disable()
	  	.authorizeRequests()
	  	/*  
	  	 * .antMatchers("/product/**").access("hasRole('ADMIN')")
	  	 * builder.roles("USER","ADMIN") is equivalent to builder.authorities("ROLE_USER","ROLE_ADMIN")  
	  	 * */
	  	.antMatchers("/api/shape/**").hasAuthority("ROLE_ADMIN")
        .anyRequest().authenticated();
    }
}
