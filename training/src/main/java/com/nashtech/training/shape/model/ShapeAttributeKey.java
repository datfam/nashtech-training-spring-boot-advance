package com.nashtech.training.shape.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShapeAttributeKey implements Serializable {

    private String attributeName;
    private long category;
}
