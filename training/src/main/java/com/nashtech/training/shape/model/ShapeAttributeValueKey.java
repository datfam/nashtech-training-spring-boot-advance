package com.nashtech.training.shape.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShapeAttributeValueKey implements Serializable {

	private static final long serialVersionUID = -6288271051165190280L;

	private String attributeName;

    private long shape;
}