package com.nashtech.training.shape.rest;

import static org.springframework.util.StringUtils.isEmpty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.nashtech.training.exception.ObjectConflictException;
import com.nashtech.training.exception.ObjectNotFoundException;
import com.nashtech.training.shape.CalcAreaException;
import com.nashtech.training.shape.dto.ShapeCategoryDto;
import com.nashtech.training.shape.dto.ShapeDto;
import com.nashtech.training.shape.model.Shape;
import com.nashtech.training.shape.model.ShapeAttribute;
import com.nashtech.training.shape.model.ShapeCategory;
import com.nashtech.training.shape.service.ShapeService;
import com.nashtech.training.shape.utils.ShapeCalculatorUtils;
import com.nashtech.training.shape.utils.ShapeMapper;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/shape")
@Slf4j
public class ShapeController {

	final private ShapeService shapeService;

	public ShapeController(ShapeService shapeService) {
		this.shapeService = shapeService;
	}

	@Operation(summary = "Get all category")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Found all category", content = @Content(mediaType = "application/json", 
					array = @ArraySchema(schema = @Schema(implementation = ShapeCategoryDto.class)))) })
	@GetMapping("/categories")
	ResponseEntity<List<ShapeCategoryDto>> getAllCategory() {
		List<ShapeCategory> categories = this.shapeService.getAllCategoryFull();
		return ResponseEntity.ok(categories.stream().map(ShapeMapper.INSTANCE::toDto).collect(Collectors.toList()));
	}

	@Operation(summary = "Get a category by its id")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Found the shape category", content = {
			@Content(mediaType = "application/json", schema = @Schema(implementation = ShapeCategoryDto.class)) }) })
	@GetMapping("/category/{catid}")
	ResponseEntity<ShapeCategoryDto> getCategory(@PathVariable("catid") long id) {
		Optional<ShapeCategory> category = this.shapeService.getCategory(id);
		if (!category.isPresent()) {
			throw new ObjectNotFoundException("Not found category with id is " + id);
		}
		
		ShapeCategoryDto result = category.map(e -> ShapeMapper.INSTANCE.toDto(e)).get();
		
		/* return the outcome */
		return ResponseEntity.ok().eTag(Integer.toString(category.get().getVersion())).body(result);
	}

	@Operation(summary = "Delete a category by its id")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Delete category", content = {
			@Content(mediaType = "text/plain", schema = @Schema(implementation = Void.class)) }) })
	@DeleteMapping("/category/{catid}")
	ResponseEntity<Void> deleteCategory(@PathVariable("catid") long id) {
		if (this.shapeService.deleteCategory(id)) {
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.badRequest().build();
	}

	@Operation(summary = "Update a category by info of category")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Update category", content = {
			@Content(mediaType = "text/plain", schema = @Schema(implementation = Long.class)) }) })
	@PostMapping("/category/save")
	ResponseEntity<Long> saveCategory(WebRequest request, @Valid @RequestBody ShapeCategoryDto shapeCategoryDto) {
		String ifMatchValue = request.getHeader("If-Match");
		int version = 0;
        if (isEmpty(ifMatchValue)) {
            return ResponseEntity.badRequest().build();
        }
        
        /* if category is exist but version is not same => throw ObjectConflictException */
        Optional<ShapeCategory> shapeCategoryEntity = this.shapeService.getCategory(shapeCategoryDto.getId());
        if (shapeCategoryEntity.isPresent()) {
        	ShapeCategory cat = shapeCategoryEntity.get();
        	if (!ifMatchValue.equals(Integer.toString(cat.getVersion()))) {
        		throw new ObjectConflictException("The version of this category is changed, please retrive before update it");
        	}
        	
        	/* get the current version of category to can able update success if category already existing */
        	version = cat.getVersion();
        }
		
		final ShapeCategory category = ShapeMapper.INSTANCE.toEntity(shapeCategoryDto);
		category.setVersion(version);
		try {
			final ShapeCategory categoryDb = this.shapeService.saveCategory(category);
			return ResponseEntity.ok().eTag(Integer.toString(categoryDb.getVersion())).body(categoryDb.getId());
		} catch (Exception ex) {
			return ResponseEntity.badRequest().build();
		}
	}

	@Operation(summary = "Calculate area of shape")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Get area of shape", content = {
			@Content(mediaType = "text/plain", schema = @Schema(implementation = Double.class)) }) })
	@PostMapping("/calc")
	ResponseEntity<Double> calcAreaShape(@Valid @RequestBody ShapeDto shapeDto) {
		Optional<ShapeCategory> category = this.shapeService.getCategory(shapeDto.getCatId());

		if (category.isPresent() && validateAttribute(category.get(), shapeDto)) {
			Map<String, Double> mapAttribute = new HashMap<>();
			shapeDto.getAttributes().entrySet().forEach(e -> mapAttribute.put(e.getKey(), e.getValue()));
			try {
				return ResponseEntity.ok(ShapeCalculatorUtils.calcArea(category.get().getAreaFormula(), mapAttribute));
			} catch (CalcAreaException e) {
				log.error("calcAreaShape", e);
			}
		}

		return ResponseEntity.badRequest().build();
	}

	@Operation(summary = "Update shape by info of shape")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Update shape", content = {
			@Content(mediaType = "text/plain", schema = @Schema(implementation = Long.class)) }) })
	@PutMapping("/save")
	ResponseEntity<Long> saveShape(WebRequest request, @Valid @RequestBody ShapeDto shapeDto) {
		String ifMatchValue = request.getHeader("If-Match");
		int version = 0;
        if (isEmpty(ifMatchValue)) {
            return ResponseEntity.badRequest().build();
        }
        
        /* if shape is exist but version is not same => throw ObjectConflictException */
        Optional<Shape> shapeOptional = this.shapeService.getShapeById(shapeDto.getId());
        if (shapeOptional.isPresent()) {
        	Shape shapeEntity = shapeOptional.get();
        	if (!ifMatchValue.equals(Integer.toString(shapeEntity.getVersion()))) {
        		throw new ObjectConflictException("The version of this shape is changed, please retrive before update it");
        	}
        	
        	/* get the current version of shape to can able update success if shape already existing */
        	version = shapeEntity.getVersion();
        }
        
		final Shape shape = ShapeMapper.INSTANCE.toEntity(shapeDto);
		shape.setVersion(version);
		
		try {
			final Shape shapeDb = this.shapeService.save(shape);
			return ResponseEntity.ok().eTag(Integer.toString(shapeDb.getVersion())).body(shapeDb.getId());
		} catch (Exception ex) {
			return ResponseEntity.badRequest().build();
		}
	}

	@Operation(summary = "Get all shape")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Found all shape", content = @Content(mediaType = "application/json", 
					array = @ArraySchema(schema = @Schema(implementation = ShapeDto.class)))) })
	@GetMapping("/all")
	ResponseEntity<List<ShapeDto>> getAllShape() {
		List<Shape> shapes = this.shapeService.getAllShape();
		return ResponseEntity.ok(shapes.stream().map(ShapeMapper.INSTANCE::toDto).collect(Collectors.toList()));
	}

	@Operation(summary = "Get a shape by its id")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Found the shape", content = {
			@Content(mediaType = "application/json", schema = @Schema(implementation = ShapeDto.class)) }) })
	@GetMapping("/{id}")
	ResponseEntity<ShapeDto> getShape(@PathVariable("id") long id) {
		Optional<Shape> shape = this.shapeService.getShapeById(id);
		
		if (!shape.isPresent()) {
			throw new ObjectNotFoundException("Not found shape with id is " + id);
		}
		
		ShapeDto shapeDto = shape.map(s -> ShapeMapper.INSTANCE.toDto(s)).get();
		
		/* return the outcome */
		return ResponseEntity.ok().eTag(Integer.toString(shape.get().getVersion())).body(shapeDto);
	}

	@Operation(summary = "Delete a shape by its id")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Delete shape", content = {
			@Content(mediaType = "text/plain", schema = @Schema(implementation = Void.class)) }) })
	@DeleteMapping("/{id}")
	ResponseEntity<Void> deleteShape(@PathVariable("id") long id) {
		if (this.shapeService.deleteShape(id)) {
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.badRequest().build();
	}

	private boolean validateAttribute(ShapeCategory category, ShapeDto shapeDto) {
		if (category.getAttributes().size() != shapeDto.getAttributes().size()) {
			return false;
		}
		Set<String> fname1 = category.getAttributes().stream().map(ShapeAttribute::getAttributeName)
				.collect(Collectors.toSet());
		Set<String> fname2 = shapeDto.getAttributes().keySet();
		return fname1.containsAll(fname2);
	}
}
