package com.nashtech.training.shape;

public class CalcAreaException extends Exception {

	static final long serialVersionUID = 7092926015941542347L;

	public CalcAreaException(String s) {
        super(s);
    }
}