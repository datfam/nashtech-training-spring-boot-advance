package com.nashtech.training.shape.dto;

import java.util.Date;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ApiError {
	private Date timestamp = new Date();
	private final String statusCode;
    private final String message;
}
