package com.nashtech.training.shape.service.impl;

import com.nashtech.training.shape.model.Shape;
import com.nashtech.training.shape.model.ShapeCategory;
import com.nashtech.training.shape.repository.ShapeCategoryRepository;
import com.nashtech.training.shape.repository.ShapeRepository;
import com.nashtech.training.shape.service.ShapeService;
import com.nashtech.training.shape.utils.LongRunning;
import lombok.extern.slf4j.Slf4j;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class ShapeServiceImpl implements ShapeService {

    final private ShapeRepository shapeRepository;
    final private ShapeCategoryRepository shapeCategoryRepository;
    final private JdbcTemplate jdbcTemplate;

    public ShapeServiceImpl(ShapeRepository shapeRepository, ShapeCategoryRepository shapeCategoryRepository, JdbcTemplate jdbcTemplate) {
        this.shapeRepository = shapeRepository;
        this.shapeCategoryRepository = shapeCategoryRepository;
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public List<ShapeCategory> getAllCategory() {
        return shapeCategoryRepository.findAll();
    }

    @Override
    public List<ShapeCategory> getAllCategoryFull() {
        return shapeCategoryRepository.findAll();
    }

    @CachePut(value = "categories", key = "#category.id")
    @Override
    public ShapeCategory saveCategory(ShapeCategory category) {
        return shapeCategoryRepository.save(category);
    }

    @Override
    public List<Shape> getAllShape() {
        return this.shapeRepository.findAll();
    }

    @Override
    public List<Shape> getAllShape(String user) {
        return this.shapeRepository.findAllWithAttributeByUser(user);
    }

    @Cacheable(value = "shapes", key = "#id", unless="#result == null")
    @Override
    public Optional<Shape> getShapeById(long id) {
        LongRunning.run();
        return this.shapeRepository.findOneWithAttributeById(id);
    }

    @Cacheable(value = "categories", key = "#id", unless="#result == null")
    @Override
    public Optional<ShapeCategory> getCategory(long id) {
        return shapeCategoryRepository.findOneWithAttributeById(id);
    }

    @CachePut(value = "shapes", key = "#shape.id")
    @Override
    public Shape save(Shape shape) {
        return this.shapeRepository.save(shape);
    }

    @CacheEvict(value = "shapes", key = "#id")
    @Override
    public boolean deleteShape(long id) {
    	try {
    		this.shapeRepository.deleteById(id);
    		return true;
		} catch (Exception e) {
			log.error("Delete shape failed, ex: ", e);
			return false;
		}
    }

    @CacheEvict(value = "categories", key = "#id")
    @Override
    public boolean deleteCategory(long id) {
    	try {
    		this.shapeCategoryRepository.deleteById(id);
    		return true;
		} catch (Exception e) {
			log.error("Delete shape category failed, ex: ", e);
			return false;
		}
    }
}