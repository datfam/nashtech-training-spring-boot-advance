package com.nashtech.training.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.nashtech.training.shape.dto.ApiError;

@ControllerAdvice
public class CustomizedResponseStatusExceptionHandler extends ResponseEntityExceptionHandler 
{
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ApiError> handleAll(final Exception ex, final WebRequest request) throws Exception {
		final ApiError error = new ApiError("unknowException", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(ObjectNotFoundException.class)
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	public final ResponseEntity<ApiError> handleEx(final ObjectNotFoundException ex, final WebRequest request) {
		final ApiError error = new ApiError("notFoundEx", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(ValidationException.class)
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public final ResponseEntity<ApiError> handleEx(final ValidationException ex, final WebRequest request) {
		final ApiError error = new ApiError("notFoundEx", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(ObjectConflictException.class)
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public final ResponseEntity<ApiError> handleEx(final ObjectConflictException ex, final WebRequest request) {
		final ApiError error = new ApiError("conflictEx", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.CONFLICT);
	}
}
