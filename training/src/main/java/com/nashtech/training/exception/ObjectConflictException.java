package com.nashtech.training.exception;

public class ObjectConflictException extends RuntimeException {

	private static final long serialVersionUID = -5932321708238099358L;

	public ObjectConflictException() {
		super();
	}

	public ObjectConflictException(String message) {
		super(message);
	}

	public ObjectConflictException(String message, Throwable cause) {
		super(message, cause);
	}

	public ObjectConflictException(Throwable cause) {
		super(cause);
	}

	protected ObjectConflictException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}